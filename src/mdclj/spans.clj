(ns mdclj.spans
  (:use [mdclj.misc]))

(def ^:private formatter 
  [["`"  ::inlinecode]
   ["**" ::strong]
   ["__" ::strong]
   ["*"  ::emphasis]
   ["_"  ::emphasis]])

(defn- apply-formatter [text [pattern spantype]]
   "Checks if text starts with the given pattern. If so, return the spantype, the text
    enclosed in the pattern, and the remaining text"
  (when-let [[body remaining] (delimited text pattern)]
      [spantype body remaining]))

(defn- get-spantype [text]
  (let [[spantype body remaining :as match] (some #(apply-formatter text %) formatter)]
    (if (some-every-pred startswith [body remaining] ["*" "_"]) 
       [spantype (-> body (vec) (conj (first remaining))) (rest remaining)]
       match)))

(defn- make-literal [acc]
  "Creates a literal span from the acc"
  {:type ::literal :content (to-string (reverse acc))})

(declare parse-spans)

(defn- span-emit [literal-text span]
  "Creates a vector containing a literal span created from literal-text and 'span' if literal-text, else 'span'"
  (if (seq literal-text)
    [(make-literal literal-text) span]  ;; if non-empty literal before next span
    [span]))

(defn- concat-spans [acc span remaining]
  (concat (span-emit acc span) (parse-spans [] remaining)))

(defn- parse-span-body
  ([body]
    (parse-span-body nil body))
  ([spantype body]
    (if (in? [::inlinecode ::image] spantype)
      (to-string body)
      (parse-spans [] body)))) ;; all spans except inlinecode and image can be nested

(defn- match-span [acc text] ;; matches ::inlinecode ::strong ::emphasis
  (when-let [[spantype body remaining :as match] (get-spantype text)] ;; get the first matching span
      (let [span {:type spantype :content (parse-span-body spantype body)}]
        (concat-spans acc span remaining))))

(defn- extract-link-title [text]
  (reduce #(clojure.string/replace % %2 "") (to-string text) [#"\"$" #"'$" #"^\"" #"^'"]))  

(defn- parse-link-text [linktext]
  (let [[link title] (clojure.string/split (to-string linktext) #" " 2)]
    (if (seq title)
      {:url link :title (extract-link-title title)}
      {:url link})))

(defn- match-link-impl [acc text type]
  (when-let [[linkbody remaining :as body] (bracketed text "[" "]")]
    (when-let [[linktext remaining :as link] (bracketed remaining "(" ")")]
      (concat-spans acc (into {:type type :content (parse-span-body type linkbody)} (parse-link-text linktext)) remaining))))
  
(defn- match-link [acc text]
  (match-link-impl acc text ::link))

(defn- match-inline-image [acc [exmark & remaining :as text]]
  (when (= exmark \!)
	  (match-link-impl acc remaining ::image)))

(defn- match-break [acc text]
  (when-let [remaining (some #(startswith text %) ["  \n\r" "  \n" "  \r"])]                ;; match hard-breaks
    (concat-spans acc {:type ::hard-break} remaining)))

(defn- match-literal [acc [t & trest :as text]]
  (cond
    (seq trest)
      (parse-spans (cons t acc) trest) ;; accumulate literal body (unparsed text left)
    (seq text)
      (list (make-literal (cons t acc))))) ;; emit literal (at end of text: no trest left)

(def ^:private span-matcher  
  [match-span 
   match-link 
   match-inline-image
   match-break 
   match-literal])

(defn parse-spans
  ([text]
    (parse-spans [] text))
  ([acc text]
   (some #(% acc text) span-matcher)))
