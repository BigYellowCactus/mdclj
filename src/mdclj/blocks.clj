(ns mdclj.blocks
  (:use [clojure.string :only [blank? split]]
        [mdclj.spans :only [parse-spans]]
        [mdclj.misc]))

(defn- collect-prefixed-lines [lines prefix]
  (when-let [[prefixed remaining] (partition-while #(startswith % prefix) lines)]
     [(map #(to-string (drop (count prefix) %)) prefixed) remaining]))

(defn- line-seperated [lines]
  (when-let [[par [r & rrest :as remaining]] (partition-while (complement blank?) lines)]
    (list par rrest)))

(declare parse-blocks)

(defn- create-block-map [type content & extra]
  (into {:type type :content content} extra))

(defn- clean-heading-string [line]
  (-> line (to-string)
           (clojure.string/trim) 
           (clojure.string/replace  #" #*$" "") ;; match space followed by any number of #s
           (clojure.string/trim)))

(defn match-heading [[head & remaining :as text]]
  (let [headings (map vector (range 1 6) (iterate #(str \# %) "#")) ;; ([1 "#"] [2 "##"] [3 "###"] ...) 
        [size rest] (some (fn [[index pattern]] 
                            (let [rest (startswith head pattern)]
                              (when (seq rest) 
                                [index rest]))) headings)]
    (when (not (nil? rest))
      [(create-block-map ::heading (parse-spans (clean-heading-string rest)) {:size size}) remaining])))

(defn- match-underline-heading [[caption underline & remaining :as text]]
  (let [current (set underline)
        marker [\- \=]
        markers (mapcat #(list #{\space %} #{%}) marker)]
    (when (and (some #(= % current) markers)
               (some #(startswith underline [%]) marker)
               (< (count (partition-by identity underline)) 3))
      [(create-block-map ::heading (parse-spans caption) remaining {:size 1}) remaining])))

(defn- match-horizontal-rule [[rule & remaining :as text]]
  (let [s (set rule)
        marker [\- \*]
        markers (mapcat #(list #{\space %} #{%}) marker)]
    (when (and (some #(= % s) markers)
               (> (some #(get (frequencies rule) %) marker) 2))
      [{:type ::hrule} remaining])))

(defn- match-codeblock [text]
  (when-let [[code remaining] (collect-prefixed-lines text "    ")]
    [(create-block-map ::codeblock code) remaining]))

(defn- match-blockquote [text]
  (when-let [[quote remaining] (collect-prefixed-lines text "> ")]
    [(create-block-map ::blockquote (parse-blocks quote)) remaining]))

(defn- match-paragraph [text]
  (when-let [[lines remaining] (line-seperated text)]
   [(create-block-map ::paragraph (parse-spans (clojure.string/join "\n" lines))) remaining]))

(defn- match-empty [[head & remaining :as text]]
  (when (and (blank? head) (seq remaining))
    (parse-blocks remaining)))

(def ^:private block-matcher  
  [match-heading 
   match-underline-heading
   match-horizontal-rule
   match-codeblock 
   match-blockquote
   match-paragraph 
   match-empty])

(defn- parse-blocks [lines]
  (lazy-seq
	  (when-let [[result remaining] (some #(% lines) block-matcher)]
	    (cons result (parse-blocks remaining)))))
  
(defn parse-text [text]
  (parse-blocks (seq (clojure.string/split-lines text))))


