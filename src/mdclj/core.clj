(ns mdclj.core
  (:use [mdclj.blocks :only [parse-text]])
  (:use [mdclj.html :only [format-blocks]]))


(defn md2html [md]
  (format-blocks (parse-text md)))

(defn -main [md]
  (println (str "<html><body>" (md2html md) "</body></html>")))
