(ns mdclj.html
  (:use [mdclj.misc]))

(defn- escape-html
  "Change special characters into HTML character entities."
  [text]
  (let [s (to-string text)]
    (if 
      (bracketed s "<" ">") s 
		  (.. s 
		    (replace "&" "&amp;")
		    (replace "<" "&lt;")
		    (replace ">" "&gt;")
		    (replace "\"" "&quot;")))))

(defn- build-attr-str [attributes]
  (let [attr (clojure.string/join \space (for [[k v] attributes :when (seq v)] (str k \= \" v \")))
        attr (if (seq attributes) (str \space attr) attr)]
    attr))

(defn output-element 
  ([tag attributes bodyf]
    (str \< tag (build-attr-str attributes) \> (bodyf) "</" tag \>))
  ([tag attributes]
    (str \< tag (build-attr-str attributes) " />"))
  ([tag]
    (str \< tag " />")))

(defn hex-encode [text]
  (to-string (map #(str "&#x" (.toUpperCase (Integer/toHexString (int %))) \;) text)))

(defmulti format-span :type)

(defn format-spans [spans]
  (clojure.string/join (map format-span spans)))

(defn- content-parser [spans]
  #(clojure.string/join (map format-span spans)))

(defmethod format-span :mdclj.spans/literal [{text :content}]
  (let [[link _ :as result] (bracketed text "<" ">")]
    (cond 
      (some #(startswith link %) ["http:" "https:" "ftp:"]) 
        (output-element "a" {"href" (to-string link)} #(escape-html link))
      (in? link \@)
        (output-element "a" {"href" (str (hex-encode "mailto") \: (hex-encode link))} #(hex-encode link))
      :else
        (escape-html text))))
 
(defmethod format-span :mdclj.spans/strong [{inner :content}]
  (output-element "strong" [] (content-parser inner)))

(defmethod format-span :mdclj.spans/emphasis [{inner :content}]
  (output-element "em" [] (content-parser inner)))

(defmethod format-span :mdclj.spans/link [{inner :content url :url title :title}]
  (output-element "a" {"href" url "title" title} (content-parser inner)))

(defmethod format-span :mdclj.spans/image [{inner :content url :url title :title}]
  (output-element "img" {"src" url "alt" inner "title" title}))

(defmethod format-span :mdclj.spans/hard-break [span]
  (output-element "br"))

(defmethod format-span :mdclj.spans/inlinecode [{code :content}]
  (str "<code>" code "</code>"))

(defmulti format-block :type)

(defn format-blocks [blocks]
  (clojure.string/join (map format-block blocks)))

(defmethod format-block :mdclj.blocks/heading [{inner :content size :size}]
  (output-element (str \h size) [] (content-parser inner)))

(defmethod format-block :mdclj.blocks/paragraph [{inner :content}]
  (output-element "p" [] (content-parser inner)))

(defmethod format-block :mdclj.blocks/blockquote [{inner :content}]
  (output-element "blockquote" [] #(format-blocks inner)))

(defmethod format-block :mdclj.blocks/codeblock [{inner :content}]
  (output-element "pre" [] #(clojure.string/join \newline inner)))

(defmethod format-block :mdclj.blocks/hrule [_]
  "<hr />")

