(ns mdclj.misc)
  
(defn in? 
  "true if seq contains elm"
  [seq elm]  
  (some #(= elm %) seq))

(defn startswith [coll prefix]
  "Checks if coll starts with prefix.
   If so, returns the rest of coll, otherwise nil"
  (let [[t & trest] coll
        [p & prest] prefix]
	  (cond
	    (and (= p t) ((some-fn seq) trest prest)) (recur trest prest)
	    (= p t) '()
	    (nil? prefix) coll)))

(defn partition-while
  ([f coll]
    (partition-while f [] coll))
  ([f acc [head & tail :as coll]]
    (cond
      (f head)
        (recur f (cons head acc) tail)
      (seq acc)
        (list (reverse acc) coll))))

(defn- bracketed-body [closing acc text]
  "Searches for the sequence 'closing' in text and returns a
   list containing the elements before and after it"
  (let [[t & trest] text
        r (startswith text closing)]
  (cond
    (not (nil? r)) (list (reverse acc) r)
    (seq text) (recur closing (cons t acc) trest))))

(defn bracketed [coll opening closing]
  "Checks if coll starts with opening and ends with closing.
   If so, returns a list of the elements between 'opening' and 'closing', and the
   remaining elements"
  (when-let [remaining (startswith coll opening)]
    (bracketed-body closing '() remaining)))

(defn delimited [coll pattern]
  "Checks if coll starts with pattern and also contains pattern.
   If so, returns a list of the elements between the pattern and the remaining elements"
  (bracketed coll pattern pattern))

(defn to-string [coll]
  "Takes a coll of chars and returns a string"
  (apply str coll))

(defn some-every-pred [f ands ors]
  "Builds a list of partial function predicates with function f and
   all values in ands and returns if any argument in ors fullfills
   all those predicates" 
  (let [preds (map #(partial f %) ands)]
    (some true? (map #((apply every-pred preds) %) ors))))