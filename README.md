# mdclj

`mdclj` is a parser for [markdown](http://daringfireball.net/projects/markdown/) written in [clojure](http://clojure.org).

## Features

`mdclj` contains a [markdown](http://daringfireball.net/projects/markdown/) parser, as well as a html formatter to generate html documents.

The following markdown features are currently supported:

+ [Paragraphs](http://daringfireball.net/projects/markdown/syntax#p)
+ [Line Breaks](http://daringfireball.net/projects/markdown/syntax#p)
+ [Headers](http://daringfireball.net/projects/markdown/syntax#header) (Setext-style and atx-style) 
+ [Blockqoutes](http://daringfireball.net/projects/markdown/syntax#blockquote)
+ [Code Blocks](http://daringfireball.net/projects/markdown/syntax#precode)
+ [Horizontal Rules](http://daringfireball.net/projects/markdown/syntax#hr)
+ [Links](http://daringfireball.net/projects/markdown/syntax#link)
+ [Emphasis](http://daringfireball.net/projects/markdown/syntax#em)
+ [Inline Code](http://daringfireball.net/projects/markdown/syntax#code)
+ [Automatic links](http://daringfireball.net/projects/markdown/syntax#autolink) (as part of the html formatter, may be changed later)

## Status

`mdclj` is still in development and does *currently not support all markdown features*, and may also contain bugs. Feel free to [submit](https://bitbucket.org/BigYellowCactus/mdclj/issues "Issue Tracker") feature requests and bug reports.

## License

Copyright (C) 2012 Dominic Kexel

Distributed under the Eclipse Public License, the same as Clojure.
