(defproject mdclj "0.1.0-SNAPSHOT"
  :description "a markdown parser (and html formatter)"
  :main mdclj.core
  :dependencies     [[org.clojure/clojure "1.4.0"]]
  :dev-dependencies [[lein-eclipse        "1.0.0"]])
