(ns mdclj.test.blocks
  (:use [mdclj.blocks])
  (:use [clojure.test]))

(deftest parse-text-test
  (let [text "# Introducing F#
F# is a _functional-first_ language,
which looks like this:

    let msg = \"world\"
    printfn \"hello %s!\" msg

This sample prints `hello world!`"]
    (is (= (parse-text text) [{:type :mdclj.blocks/heading, :size 1, :content [{:type :mdclj.spans/literal :content "Introducing F#"}]}
																		 {:type :mdclj.blocks/paragraph,
																		  :content
																		  [{:type :mdclj.spans/literal, :content "F# is a "}
																		   {:type :mdclj.spans/emphasis,
																		    :content [{:type :mdclj.spans/literal, :content "functional-first"}]}
																		   {:type :mdclj.spans/literal, :content " language,\nwhich looks like this:"}]}
																		 {:type :mdclj.blocks/codeblock,
																		  :content ["let msg = \"world\"" "printfn \"hello %s!\" msg"]}
																		 {:type :mdclj.blocks/paragraph,
																		  :content
																		  [{:type :mdclj.spans/literal, :content "This sample prints "}
																		   {:type :mdclj.spans/inlinecode, :content "hello world!"}]}]))))

(deftest atx-heading
    (let [valid [{:type :mdclj.blocks/heading, 
                 :content [{:type :mdclj.spans/literal, :content "123"}], :size 1}]]
      (is (= (parse-text "# 123") valid))
      (is (= (parse-text "# 123 #") valid))
      (is (= (parse-text "#123 ##") valid))
      (is (= (parse-text "#123") valid))
      (is (= (parse-text "#123   ####") valid))
      (is (= (parse-text "#123 #") valid))
      (is (= (parse-text "#123 F#") [{:type :mdclj.blocks/heading, 
                                      :content [{:type :mdclj.spans/literal, :content "123 F#"}], :size 1}]))))

(deftest horizontal-rule
  (let [hrule {:type :mdclj.blocks/hrule}]
    (is (= (parse-text "---") [hrule]))
    (is (= (parse-text "***") [hrule]))
    (is (= (parse-text "*********") [hrule]))
    (is (= (parse-text "*  *  *") [hrule]))
    (is (= (parse-text "- - -") [hrule]))
    (is (= (parse-text "*  *  *          ") [hrule]))))

(deftest underline-heading
  (let [valid [{:type :mdclj.blocks/heading, 
                 :content [{:type :mdclj.spans/literal, :content "123"}], :size 1}]]
    (is (= (parse-text "123\n----") valid))
    (is (= (parse-text "123\n=") valid))
    (is (= (parse-text "123\n-") valid))
    (is (= (parse-text "123\n---")  valid))
    (is (= (parse-text "123\n== ==") [{:type :mdclj.blocks/paragraph, 
                                       :content [{:type :mdclj.spans/literal, :content "123\n== =="}]}]))))
    

(deftest blockquote
  (let [text "> block\n> **strong**\nstuff"]
    (is (= (parse-text text) [{:type :mdclj.blocks/blockquote, 
                               :content 
                               [{:type :mdclj.blocks/paragraph, 
                                 :content
                                 [{:type :mdclj.spans/literal, :content "block\n"}
                                  {:type :mdclj.spans/strong, 
                                   :content [{:type :mdclj.spans/literal, :content "strong"}]}]}]}
                              {:type :mdclj.blocks/paragraph, 
                               :content [{:type :mdclj.spans/literal, :content "stuff"}]}]))))