(ns mdclj.test.html
  (:use [mdclj.html])
  (:use [mdclj.blocks :only [parse-text]])
  (:use [clojure.test]))


(defn- body []
  "foobar")

(deftest create-html-element
  (is (= (output-element "Foo" {"Attr1" "12" "Attr2" "stuff"} body)
         "<Foo Attr1=\"12\" Attr2=\"stuff\">foobar</Foo>")))

(deftest escaping
  (let [text   "So < what & is \" up ?"
        result "<p>So &lt; what &amp; is &quot; up ?</p>"]
    (is (= (format-blocks (parse-text text)) result)))) 

(deftest parse-html-text
  (let [text "# Introducing F#
F# is a _functional-first_ language,
which looks like this:

    let msg = \"world\"
    printfn \"hello %s!\" msg

This sample prints `hello world!`"]
  (is (= (format-blocks (parse-text text)) "<h1>Introducing F#</h1><p>F# is a <em>functional-first</em> language,\nwhich looks like this:</p><pre>let msg = \"world\"\nprintfn \"hello %s!\" msg</pre><p>This sample prints <code>hello world!</code></p>"
))))

(deftest parse-blockqoute
  (let [text "> block"]
    (is (= (format-blocks (parse-text text)) "<blockquote><p>block</p></blockquote>"))))

(deftest parse-hrules
  (is (= (format-blocks (parse-text "123\n\n---\n")) "<p>123</p><hr />")))

(deftest parse-urls
  (is (= (format-blocks (parse-text "[Example](http://example.com Click Me!)")) "<p><a href=\"http://example.com\" title=\"Click Me!\">Example</a></p>"))
  (is (= (format-blocks (parse-text "[Example](http://example.com \"Click Me!\")")) "<p><a href=\"http://example.com\" title=\"Click Me!\">Example</a></p>"))
  (is (= (format-blocks (parse-text "[Example](http://example.com 'Click Me!')")) "<p><a href=\"http://example.com\" title=\"Click Me!\">Example</a></p>"))
  (is (= (format-blocks (parse-text "[Example](http://example.com)")) "<p><a href=\"http://example.com\">Example</a></p>"))
  (is (= (format-blocks (parse-text "![](http://example.com)"))                      "<p><img src=\"http://example.com\" /></p>"))
  (is (= (format-blocks (parse-text "![Example](http://example.com \"Click Me!\")")) "<p><img src=\"http://example.com\" alt=\"Example\" title=\"Click Me!\" /></p>"))
  (is (= (format-blocks (parse-text "![Example](http://example.com 'Click Me!')"))   "<p><img src=\"http://example.com\" alt=\"Example\" title=\"Click Me!\" /></p>")))

(deftest parse-urls1
  (let [[http-text  http-result]  ["<http://stuff>"  "<p><a href=\"http://stuff\">http://stuff</a></p>"]]
    (is (= (format-blocks (parse-text http-text))  http-result))))

(deftest parse-urls2
  (let [[https-text https-result] ["<https://stuff>" "<p><a href=\"https://stuff\">https://stuff</a></p>"]]
    (is (= (format-blocks (parse-text https-text)) https-result))))

(deftest parse-urls3
  (let [[ftp-text   ftp-result]   ["<ftp://stuff>"   "<p><a href=\"ftp://stuff\">ftp://stuff</a></p>"]]
    (is (= (format-blocks (parse-text ftp-text))   ftp-result))))

(deftest parse-urls4
  (let [[fail-text  fail-result]  ["<stuff:test>"    "<p><stuff:test></p>"]]
    (is (= (format-blocks (parse-text fail-text))  fail-result))))

(deftest parse-urls5
  (let [[mail-text  mail-result]  ["<address@example.com>" "<p><a href=\"&#x6D;&#x61;&#x69;&#x6C;&#x74;&#x6F;:&#x61;&#x64;&#x64;&#x72;&#x65;&#x73;&#x73;&#x40;&#x65;&#x78;&#x61;&#x6D;&#x70;&#x6C;&#x65;&#x2E;&#x63;&#x6F;&#x6D;\">&#x61;&#x64;&#x64;&#x72;&#x65;&#x73;&#x73;&#x40;&#x65;&#x78;&#x61;&#x6D;&#x70;&#x6C;&#x65;&#x2E;&#x63;&#x6F;&#x6D;</a></p>"]]
    (is (= (format-blocks (parse-text mail-text))  mail-result))))

(deftest parse-reference-style-links
  (let [expected "<p>I get 10 times more traffic from <a href=\"http://google.com/\" title=\"Google\">Google</a> than from <a href=\"http://search.yahoo.com/\" title=\"Yahoo Search\">Yahoo</a> or <a href=\"http://search.msn.com/\" title=\"MSN Search\">MSN</a>.</p>"
        reference-links 
"I get 10 times more traffic from [Google] [1] than from [Yahoo] [2] or [MSN] [3].

  [1]: http://google.com/        \"Google\"
  [2]: http://search.yahoo.com/  \"Yahoo Search\"
  [3]: http://search.msn.com/    \"MSN Search\""
        implicit-link-name
"I get 10 times more traffic from [Google][] than from [Yahoo][] or [MSN][].

  [google]: http://google.com/        \"Google\"
  [yahoo]:  http://search.yahoo.com/  \"Yahoo Search\"
  [msn]:    http://search.msn.com/    \"MSN Search\""
        inline-link
"I get 10 times more traffic from [Google](http://google.com/ \"Google\") than from [Yahoo](http://search.yahoo.com/ \"Yahoo Search\") or [MSN](http://search.msn.com/ \"MSN Search\")."]
    (is (= (format-blocks (parse-text reference-links)) expected))
    (is (= (format-blocks (parse-text implicit-link-name)) expected))
    (is (= (format-blocks (parse-text inline-link)) expected))))
    
