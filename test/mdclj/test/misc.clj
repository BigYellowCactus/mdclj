(ns mdclj.test.misc
  (:use [mdclj.misc])
  (:use [clojure.test]))

(deftest startswith-test
  (is (= (to-string (startswith "**123**"    "**")) "123**"))
  (is (= (to-string (startswith "*test**"     "*")) "test**"))
  (is (= (to-string (startswith "*"           "*")) ""))
  (is (= (to-string (startswith "**test**"    "*")) "*test**"))
  (is (= (to-string (startswith "???test**" "???")) "test**"))
  (is (= (startswith "test"        "*") nil))
  (is (= (startswith "*test"      "**") nil))
  (is (= (startswith "*"      "**") nil))
  (is (= (startswith " "      "  stuff") nil)))

(deftest bracketed-text
  (is (= (->(bracketed "*test*" "*" "*")     (first) (to-string)) "test"))
  (is (nil? (bracketed "*test*" "?" "*")))
  (is (= (->(bracketed "__test__" "__" "__") (first) (to-string)) "test"))
  (is (= (->(bracketed "?=test=?" "?=" "=?") (first) (to-string)) "test"))
  (is (= (->(bracketed "?=test=?blabla" "?=" "=?") (second) (to-string)) "blabla"))
)

(deftest delimited-text
  (is (= (->(delimited "*test*" "*")     (first) (to-string)) "test"))
  (is (nil? (delimited "*test*" "?")))
  (is (= (->(delimited "__test__" "__") (first) (to-string)) "test"))
  (is (= (->(delimited "?test?blabla" "?") (second) (to-string)) "blabla"))
  )