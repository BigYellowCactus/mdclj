(ns mdclj.test.spans
  (:use [mdclj.spans])
  (:use [clojure.test]))

(def get-spantype (ns-resolve 'mdclj.spans 'get-spantype)) 

(deftest apply-formatter-test
  (is (= (get-spantype "_trtr_343243f") [:mdclj.spans/emphasis '(\t \r \t \r) '(\3 \4 \3 \2 \4 \3 \f)]))
  (is (= (get-spantype "**STRONG** and not strong") [:mdclj.spans/strong '(\S \T \R \O \N \G) '(\space \a \n \d \space \n \o \t \space \s \t \r \o \n \g)]))
  (is (= (get-spantype "**STRONG_emp_**rest") [:mdclj.spans/strong '(\S \T \R \O \N \G \_ \e \m \p \_) '(\r \e \s \t)]))
  )

(deftest parse-spans-test
  (is (= (parse-spans "t")   '({:type :mdclj.spans/literal, :content "t"})))
  (is (= (parse-spans " ")   '({:type :mdclj.spans/literal, :content " "})))
  (is (= (parse-spans "*t*") '({:type :mdclj.spans/emphasis, :content ({:type :mdclj.spans/literal, :content "t"})})))
  (is (= (parse-spans "* *") '({:type :mdclj.spans/emphasis, :content ({:type :mdclj.spans/literal, :content " "})})))
  (is (= (parse-spans "__strong *emp* __literal `code **literal**`") '({:type :mdclj.spans/strong, :content
                                                                            ({:type :mdclj.spans/literal, :content "strong "}
                                                                             {:type :mdclj.spans/emphasis, :content
                                                                                ({:type :mdclj.spans/literal, :content "emp"})}
                                                                             {:type :mdclj.spans/literal, :content " "})}
                                                                         {:type :mdclj.spans/literal, :content "literal "}
                                                                         {:type :mdclj.spans/inlinecode, :content "code **literal**"})))
  (is (= (parse-spans "hello  \n\rworld  \r!!") '({:type :mdclj.spans/literal, :content "hello"} {:type :mdclj.spans/hard-break} {:type :mdclj.spans/literal, :content "world"} {:type :mdclj.spans/hard-break} {:type :mdclj.spans/literal, :content "!!"})))
  (is (= (parse-spans "[google](http://google.com)")  '({:type :mdclj.spans/link, :url "http://google.com", :content ({:type :mdclj.spans/literal, :content "google"})})))
  (is (= (parse-spans "![google](http://google.com)") '({:type :mdclj.spans/image, :url "http://google.com", :content "google"})))
  (is (= (parse-spans "[google](http://google.com \"Click Me!\")") '({:type :mdclj.spans/link, 
                                                                      :url "http://google.com", 
                                                                      :title "Click Me!",
                                                                      :content ({:type :mdclj.spans/literal, :content "google"})})))
  (is (= (parse-spans "![google](http://google.com \"Click Me!\")") '({:type :mdclj.spans/image, 
                                                                      :url "http://google.com", 
                                                                      :title "Click Me!",
                                                                      :content "google"})))
  (is (= (parse-spans "***strongem***") '({:type :mdclj.spans/strong, :content 
                                              ({:type :mdclj.spans/emphasis, :content 
                                                ({:type :mdclj.spans/literal, :content "strongem"})})})))
  (is (= (parse-spans "**_strongem_**") '({:type :mdclj.spans/strong, :content 
                                              ({:type :mdclj.spans/emphasis, :content 
                                                ({:type :mdclj.spans/literal, :content "strongem"})})})))
  )
 